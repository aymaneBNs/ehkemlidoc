package com.ehkemli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EhkemliDocStdApplication {

	public static void main(String[] args) {
		SpringApplication.run(EhkemliDocStdApplication.class, args);
	}

}
